# Double pendulum formula translated from the C code at
# http://www.physics.usyd.edu.au/~wheat/dpend_html/solve_dpend.c

from numpy import sin, cos, pi, array
import numpy as np
import matplotlib.pyplot as plt
import scipy.integrate as integrate
import matplotlib.animation as animation
class Pendulum_Simulation:
    def __init__(self):
        self._G =  9.8 # acceleration due to gravity, in m/s^2
        self._L1 = 1.0 # length of pendulum 1 in m
        self._L2 = 1.0 # length of pendulum 2 in m
        self._M1 = 1.0 # mass of pendulum 1 in kg
        self._M2 = 1.0 # mass of pendulum 2 in kg
        self._generate_figure()
        self._generate_plot_objects()

    def _generate_figure(self):
        self._figure = plt.figure()
        self._ax = self._figure.add_subplot(
            111, autoscale_on=False, xlim=(-2, 2), ylim=(-2, 2))
        self._ax.grid()

    def _generate_plot_objects(self):
        self._animated_line, = self._ax.plot([], [], 'o-', lw=2)
        self._time_template = 'time = %.1fs'
        self._time_text = self._ax.text(
            0.05, 0.9, '', transform=self._ax.transAxes)

    def get_figure(self):
        return self._figure

    def _derivs(self, state, t):

        dydx = np.zeros_like(state)
        dydx[0] = state[1]

        del_ = state[2]-state[0]
        den1 = ((self._M1+self._M2)*self._L1 
                - self._M2*self._L1*cos(del_)*cos(del_))
        dydx[1] = (self._M2*self._L1*state[1]*state[1]*sin(del_)*cos(del_)
                   + self._M2*self._G*sin(state[2])*cos(del_) + 
                   self._M2*self._L2*state[3]*state[3]*sin(del_)
                   - (self._M1+self._M2)*self._G*sin(state[0]))/den1

        dydx[2] = state[3]

        den2 = (self._L2/self._L1)*den1
        dydx[3] = (-self._M2*self._L2*state[3]*state[3]*sin(del_)*cos(del_)
                   + (self._M1+self._M2)*self._G*sin(state[0])*cos(del_)
                   - (self._M1+self._M2)*self._L1*state[1]*state[1]*sin(del_)
                   - (self._M1+self._M2)*self._G*sin(state[2]))/den2
        return dydx

    def simulate(self):

        # create a time array from 0..100 sampled at 0.1 second steps
        self._dt = 0.05
        t = np.arange(0.0, 20, self._dt)

        # th1 and th2 are the initial angles (degrees)
        # w10 and w20 are the initial angular velocities (degrees per second)
        th1 = 120.0
        w1 = 0.0
        th2 = -10.0
        w2 = 0.0

        rad = pi/180

        # initial state
        state = np.array([th1, w1, th2, w2])*pi/180.

        # integrate your ODE using scipy.integrate.
        self._y = integrate.odeint(self._derivs, state, t)

        self._x1 = self._L1*sin(self._y[:,0])
        self._y1 = -self._L1*cos(self._y[:,0])

        self._x2 = self._L2*sin(self._y[:,2]) + self._x1
        self._y2 = -self._L2*cos(self._y[:,2]) + self._y1

        
    def _init_animation_objects(self):
        self._animated_line.set_data([], [])
        self._time_text.set_text('')
        return self._animated_line, self._time_text

    def _animate(self,i):
        thisx = [0, self._x1[i], self._x2[i]]
        thisy = [0, self._y1[i], self._y2[i]]

        self._animated_line.set_data(thisx, thisy)
        self._time_text.set_text(self._time_template%(i*self._dt))
        return self._animated_line, self._time_text

    def get_y(self):
        return self._y
    def get_y1(self):
        return self._y1
    def get_y2(self):
        return self._y2
    def get_x1(self):
        return self._x1
    def get_x2(self):
        return self._x2

    def get_G(self):
        return self._G
    def get_L1(self):
        return self._L1
    def get_L2(self):
        return self._L2
    def get_M1(self):
        return self._M1
    def get_M2(self):
        return self._M2
    def set_G(self,new_G):
        self._G=new_G
    def set_L1(self,new_L1):
        self._L1=new_L1
    def set_L2(self,new_L2):
        self._L2=new_L2
    def set_M1(self,new_M1):
        self._M1=new_M1
    def set_M2(self,new_M2):
        self._M2=new_M2

    def begin_animation(self):
        self._ani = animation.FuncAnimation(
            self._figure, self._animate, np.arange(1, len(self._y)),
            interval=25, blit=True, init_func=self._init_animation_objects)
        #ani.save('double_pendulum.mp4', fps=15, clear_temp=True)
        

def main():
    sim=Pendulum_Simulation()
    # sim.simulate()
    # plt.show() # we could plot here if we want.
    sim.set_G(-sim.get_G())
    sim.simulate()
    sim.begin_animation()
    plt.show()

if __name__=="__main__":
    main()