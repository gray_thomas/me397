# -*- coding: utf-8 -*-
"""
Created on Sun Sep 28 17:39:34 2014

@author: Jose Capriles
"""


import Tkinter as tk

import numpy as np
import matplotlib
matplotlib.use('TkAgg')

from numpy import sin, pi, cos

from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import matplotlib.pyplot as plt
import matplotlib.animation as animation

import scipy.integrate as integrate

TITLE_FONT = ("Helvetica", 10)

class Application(tk.Frame):

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.grid()
        self.pack(side="top", fill="both", expand=True)
        self.grid_rowconfigure(0, weight=1)
        self.grid_columnconfigure(0, weight=1)

        self.createWidgets()
        self.pack()
        self.font = ("Helvetica", 18, "bold")
        
        #Parameters for the simulation
        self.g = 9.81 
        self.links = 2
        self.ic = np.zeros((self.links,0))
        
        self.fig = plt.figure()

    def show_values(self):
        print "Theta,L1,M1,Theta_dot = ", (self.angle.get() , self.l1.get(), self.m1.get(), self.angle_vel.get())


    def createWidgets(self):

        #Create sliders       
        self.label_angle = tk.Label(self,text="Angle", font=TITLE_FONT).grid(column=1,row=1)
        self.angle = tk.Scale(self, from_=0.00, to=180, resolution=1)
        self.angle.grid(column=1, row = 2)
        self.angle.set(120)

        self.label_l1 = tk.Label(self,text="Lenght", font=TITLE_FONT).grid(column=2,row=1)
        self.l1 = tk.Scale(self, from_=0.00, to=2.5, resolution=0.01)
        self.l1.grid(column=2, row = 2)
        self.l1.set(1.0)
       
        self.label_m1 = tk.Label(self,text="Mass", font=TITLE_FONT).grid(column=3,row=1)
        self.m1 = tk.Scale(self, from_=0.00, to=10, resolution=0.1)
        self.m1.grid(column=3, row = 2)
        self.m1.set(1.0)

        self.label_f1 = tk.Label(self,text="Friction", font=TITLE_FONT).grid(column=4,row=1)
        self.f1 = tk.Scale(self, from_=0.00, to=1, resolution=0.01)
        self.f1.grid(column=4, row = 2)
        self.f1.set(0.1)

        self.label_anglevel = tk.Label(self,text="Ang Vel", font=TITLE_FONT).grid(column=5,row=1)
        self.angle_vel = tk.Scale(self, from_=-5.00, to=5, resolution=0.1)
        self.angle_vel.grid(column=5, row = 2)
        self.angle_vel.set(0.0)
        
        
        #Create buttons                
        self.quit = tk.Button(self, text="Quit", command=self.quit).grid(column=0,row=0)
        self.plot = tk.Button(self, text="Plot", command=self.plot).grid(column=6,row=2)
        #self.show = tk.Button(self, text='Show ICs', command=self.show_values).grid(column=6, row=2)        

        self.label_tinit = tk.Label(self, text="time = ", font=TITLE_FONT).grid(column=6,row=0)
        self.label_tinit = tk.Label(self, text="dt = ", font=TITLE_FONT).grid(column=6,row=1)
                        
        self.time = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.time).grid(column=7,row=0)
        self.time.set("10.0")
        
        self.dt_entry = tk.StringVar()
        entry = tk.Entry(self,textvariable=self.dt_entry).grid(column=7,row=1)
        self.dt_entry.set("0.05")
                
                        

    def plot(self):
        
        self.fig.clear()
        self.t = None
 
        self.dt = float(self.dt_entry.get())
        self.t = np.arange(0.0, float(self.time.get()), self.dt)
    
        #Current mass and legnth values for this plot        
        self.L1i = self.l1.get()
        self.M1i = self.m1.get()
        self.f1i = self.f1.get()
        
        # th1 is the initial angle (degrees)
        # w10 is the initial angular velocity (degrees per second)
        th1 = self.angle.get()
        w1 = self.angle_vel.get() 
 
        rad = pi/180

        # initial state
        state = np.array([th1, w1])*pi/180.

        # integrate your ODE using scipy.integrate.
        y = integrate.odeint(self.equation, state, self.t)

        self.x1 = self.L1i*sin(y[:,0])
        self.y1 = -self.L1i*cos(y[:,0])


        canvas = FigureCanvasTkAgg(self.fig, master=self)
        canvas.get_tk_widget().grid(column=0, row= 10, columnspan=10)

        ax = self.fig.add_subplot(111, autoscale_on=False, xlim=(-2.5, 2.5), ylim=(-2.5, 2.5))
        ax.grid()

        self.line, = ax.plot([], [], 'o-', lw=2)
        self.line.set_data([], [])

        self.time_template = 'time = %.1fs'
        self.time_text = ax.text(0.05, 0.9, '', transform=ax.transAxes)
        self.time_text.set_text('')

        
        self.ani = animation.FuncAnimation(self.fig, self.animate, np.arange(1, len(y)),
                                      interval=25, blit=False, init_func=self.init, repeat=False)

        #ani.save('double_pendulum.mp4', fps=15)
       
        canvas.show()
        
        
    def init(self):
        return self.line, self.time_text

    def animate(self,i):
        self.thisx = [0, self.x1[i]]
        self.thisy = [0, self.y1[i]]
        self.line.set_data(self.thisx, self.thisy)
        self.time_text.set_text(self.time_template%(i*self.dt))
        return self.line, self.time_text


    #set number of links        
    def set_number_links(self):
        pass 
    
    #set the initial conditions
    def set_ic(self):
        pass
    
    #Set the gravity
    def set_gravity(self):
        pass
        
    #We can use the same function to change all the parameters (dictionary, id=>variable)
    def change_parameter(self):
        pass

    #Re-run simulation
    def rerun(self):
        pass
    
    # Function to run the forward dynamics simulation
    # if arguments are None the function will use the ic and gravity from the class
    def run_simulation(self, ic=None, gravity=None):
        pass
    
    
    def equation(self, state, t):
        dydx = np.zeros_like(state)
        dydx[0] = state[1]
        dydx[1] = -(2*self.g/self.L1i)*np.sin(state[0]) - (self.f1i/self.M1i)*state[1]
        return dydx



root = tk.Tk()

app = Application(master=root)
app.master.title("Pendulum simulation")
app.mainloop()

root.destroy()