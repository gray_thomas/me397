import Tkinter as tk
import Tkconstants as tkc
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
import numpy as np 
import matplotlib.pyplot as plt 
import matplotlib.animation as animation
import Pendulum as pend 
class MyApp:
	def __init__(self):
		self._root=tk.Tk()
		self._generate_left_frame(self._root).pack(side=tkc.LEFT)
		self._generate_plot_frame(self._root).pack(side=tkc.LEFT)
		self._generate_right_frame(self._root).pack(side=tkc.LEFT)
		# this following line sets up python to close when you press 
		# the operating system window's exit button
		self._root.protocol('WM_DELETE_WINDOW', lambda root=self._root: exit())

	def _generate_right_frame(self, parent):
		self._right_frame=tk.Frame(master=parent)
		tk.Label(master=self._right_frame, text="Label C").pack()
		tk.Label(master=self._right_frame, text="Label D").pack()
		self._spinbox=tk.Spinbox(master=self._right_frame, 
			values=range(1,20), command=self._print_spinbox)
		self._spinbox.pack()
		return self._right_frame

	def _generate_plot_frame(self,parent):
		plot_frame=tk.Frame(master=parent)
		self._sim = pend.Pendulum_Simulation()
		self._sim.simulate()
		tk.Label(master=plot_frame, text="plot").pack()

		# self._fig=plt.figure()
		canvas = FigureCanvasTkAgg(self._sim.get_figure(), master=plot_frame)

		# ax=self._fig.add_subplot(111)
		# self._x = np.arange(0,2*np.pi, 0.01)
		# self._animated_line, = ax.plot(self._x, np.sin(self._x))

		canvas.get_tk_widget().pack()

		return plot_frame

	# def _animate(self, i):
	# 	self._animated_line.set_ydata(np.sin(self._x+i/10.0))
	# 	return self._animated_line,


	def _print_spinbox(self):
		print self._spinbox.get()
	def _hide_right_frame(self):
		self._right_frame.pack_forget()

	def _show_right_frame(self):
		self._right_frame.pack()

	def _generate_left_frame(self, parent):
		left_frame=tk.Frame(master=parent)
		tk.Button(master=left_frame, text="show",
			command=self._show_right_frame).pack()
		tk.Button(master=left_frame, text="hide"
			,command=self._hide_right_frame).pack()
		return left_frame

	def start(self):
		self._sim.begin_animation()
		# ani=animation.FuncAnimation(self._fig, self._animate,
		# 	np.arange(1,200), interval=25, blit=False)
		self._root.mainloop()
if __name__=="__main__":
	app=MyApp()
	app.start()
	print "you will never see this message"