#---------Imports
from numpy import arange, sin, pi
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg
from matplotlib.figure import Figure
import Tkinter as tk
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import double_pendulum as dp
#---------End of imports

class Foo:
    def __init__(self):
        self._bar = 'hello'

    @property
    def bar(self):
        return self._bar

    @bar.setter
    def bar(self, new):
        self._bar = new

class Application(tk.Frame):

    def say_hi(self):
        print "hi there, everyone!"

    def createWidgets(self):    
        self.add_button_panel(self).pack({"side":"left"})
        self.add_animation_canvas(self).pack({"side":"left"})
        FigureCanvasTkAgg(self.sim_fig, master=self).get_tk_widget().pack()
        # self.fig.show()

    def re_simulate(self):
        self.sim.G=self.G_slider.get()
        self.sim.L1=self.L1_slider.get()
        self.sim.L2=self.L2_slider.get()
        self.sim.M1=self.M1_slider.get()
        self.sim.M2=self.M2_slider.get()
        self.sim.simulate()
        self.theta_1_line.set_ydata(self.sim.get_data()[:,0])
        self.theta_2_line.set_ydata(self.sim.get_data()[:,1])
        self.theta_dot_1_line.set_ydata(self.sim.get_data()[:,self.sim.get_data_indexes()["point 1 x"]])
        self.theta_dot_2_line.set_ydata(self.sim.get_data()[:,self.sim.get_data_indexes()["point 2 x"]])
        self.theta_1_ax.relim()
        self.theta_2_ax.relim()
        self.theta_dot_1_ax.relim()
        self.theta_dot_2_ax.relim()


    def add_button_panel(self,parent):
        button_pannel=tk.Frame(master=self)
        self.add_quit_button(button_pannel).pack({"side": "bottom"})
        self.add_quit_button(button_pannel).pack({"side": "bottom"})
        self.add_hello_button(button_pannel).pack({"side": "bottom"})
        self.add_resimulate_button(button_pannel).pack()
        self.add_G_slider(button_pannel).pack()
        self.add_L1_slider(button_pannel).pack()
        self.add_L2_slider(button_pannel).pack()
        self.add_M1_slider(button_pannel).pack()
        self.add_M2_slider(button_pannel).pack()
        # self.w2 = tk.Scale(self, from_=0.00, to=2.00, orient=tk.HORIZONTAL, resolution=0.01)
        # self.w2.grid(column=5, row = 1)
        # self.w2.set(1.0)

        return button_pannel

    def add_G_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="gravity"
        text.pack({"side":"left"})
        self.G_slider = tk.Scale(frame, from_=0.0, to=9.8, orient=tk.HORIZONTAL, resolution=0.01)
        self.G_slider.pack()
        self.G_slider.set(9.8)
        return frame

    def add_L1_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="length 1"
        text.pack({"side":"left"})
        self.L1_slider = tk.Scale(frame, from_=0.5, to=3.0, orient=tk.HORIZONTAL, resolution=0.01)
        self.L1_slider.pack()
        self.L1_slider.set(1.0)
        return frame

    def add_L2_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="length 2"
        text.pack({"side":"left"})
        self.L2_slider = tk.Scale(frame, from_=0.5, to=3.0, orient=tk.HORIZONTAL, resolution=0.01)
        self.L2_slider.pack()
        self.L2_slider.set(1.0)
        return frame

    def add_M1_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="mass 1"
        text.pack({"side":"left"})
        self.M1_slider = tk.Scale(frame, from_=0.5, to=3.0, orient=tk.HORIZONTAL, resolution=0.01)
        self.M1_slider.pack()
        self.M1_slider.set(1.0)
        return frame

    def add_M2_slider(self,parent):
        frame=tk.Frame(parent)
        text=tk.Label(frame)
        text["text"]="mass 2"
        text.pack({"side":"left"})
        self.M2_slider = tk.Scale(frame, from_=0.5, to=3.0, orient=tk.HORIZONTAL, resolution=0.01)
        self.M2_slider.pack()
        self.M2_slider.set(1.0)
        return frame

    def add_quit_button(self,parent):
        quit_button = tk.Button(parent)
        quit_button["text"] = "QUIT"
        quit_button["fg"]   = "red"
        quit_button["command"] =  self.quit
        return quit_button

    def add_hello_button(self,parent):
        hi_there = tk.Button(parent)
        hi_there["text"] = "Hello",
        hi_there["command"] = self.say_hi
        return hi_there

    def add_resimulate_button(self,parent):
        resim = tk.Button(parent)
        resim["text"] = "Re-Simulate",
        resim["command"] = self.re_simulate
        return resim

    # def run_new_simulation(self, parameters):
    #     self.data=

    def add_animation_canvas(self,parent):
        fig = plt.Figure()

        self.x = np.arange(0, 2*np.pi, 0.01)        # x-array
        self.canvas = FigureCanvasTkAgg(fig, master=self)

        self.theta_1_ax = fig.add_subplot(411)
        self.theta_1_line, = self.theta_1_ax.plot(self.sim.get_data()[:,4], self.sim.get_data()[:,0])
        self.theta_1_dot, = self.theta_1_ax.plot(0,0,'ro')
        self.theta_1_ax.set_xlabel("$t$")
        self.theta_1_ax.set_ylabel(r"$\theta_{1}$")

        self.theta_2_ax = fig.add_subplot(412)
        self.theta_2_line, = self.theta_2_ax.plot(self.sim.get_data()[:,4], self.sim.get_data()[:,1])
        self.theta_2_dot, = self.theta_2_ax.plot(0,0,'ro')
        self.theta_2_ax.set_xlabel("$t$")
        self.theta_2_ax.set_ylabel(r"$\theta_{2}$")

        self.theta_dot_1_ax = fig.add_subplot(413)
        self.theta_dot_1_line, = self.theta_dot_1_ax.plot(self.sim.get_data()[:,4], self.sim.get_data()[:,self.sim.get_data_indexes()["point 1 x"]])
        self.theta_dot_1_dot, = self.theta_dot_1_ax.plot(0,0,'ro')
        self.theta_dot_1_ax.set_xlabel("$t$")
        self.theta_dot_1_ax.set_ylabel(r"$P_{1x}$")

        self.theta_dot_2_ax = fig.add_subplot(414)
        self.theta_dot_2_line, = self.theta_dot_2_ax.plot(self.sim.get_data()[:,4], self.sim.get_data()[:,self.sim.get_data_indexes()["point 2 x"]])
        self.theta_dot_2_dot, = self.theta_dot_2_ax.plot(0,0,'ro')
        self.theta_dot_2_ax.set_xlabel("$t$")
        self.theta_dot_2_ax.set_ylabel(r"$P_{2x}$")

        self.num_data=int((self.sim.Tf-self.sim.T0)/self.sim.dt)
        self.ani = animation.FuncAnimation(fig, self.animate, np.arange(0, self.num_data), interval=50, blit=False)
        return self.canvas.get_tk_widget()

    def animate(self,i):
        t=i*float(1.0/self.num_data)*(self.sim.Tf-self.sim.T0)
        
        # self.theta_1_line.set_ydata(np.sin(self.x+t))  # update the datas
        self.theta_1_dot.set_data(t,self.sim.get_data()[i,0])
        self.theta_2_dot.set_data(t,self.sim.get_data()[i,1])
        self.theta_dot_1_dot.set_data(t,self.sim.get_data()[i,self.sim.get_data_indexes()["point 1 x"]])
        self.theta_dot_2_dot.set_data(t,self.sim.get_data()[i,self.sim.get_data_indexes()["point 2 x"]])
        # self.theta_1_dot.set_ydata()
        return self.theta_1_line, self.theta_2_line

    def __init__(self, master=None):
        tk.Frame.__init__(self, master)
        self.pack()
        self.sim = dp.Pendulum_Simulation()
        self.sim_fig=self.sim.generate_figure()
        self.sim.init()
        self.sim.simulate()
        self.createWidgets()
        self.sim.begin_animation()

# root = tk.Tk()

def main():
    root = tk.Tk()
    app = Application(master=root)
    app.mainloop()
    root.destroy()
    

if __name__ == "__main__":
    main()

