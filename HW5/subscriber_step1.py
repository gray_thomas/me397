import sys
import zmq

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)
    
if len(sys.argv) > 2:
    port1 =  sys.argv[2]
    int(port1)

# Socket to talk to server
context = zmq.Context()
socket = context.socket(zmq.SUB)

print "Listening in to the broadcast..."
socket.connect ("tcp://localhost:%s" % port)

if len(sys.argv) > 2:
    socket.connect ("tcp://localhost:%s" % port1)

# Our message topic is 42
topicfilter = "42"
socket.setsockopt(zmq.SUBSCRIBE, topicfilter)

for update_nbr in range (20):
    string = socket.recv()
    words = string.split()[1:]
    print " ".join(words)
   