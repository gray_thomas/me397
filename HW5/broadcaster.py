import zmq
import sys
import time
import io
import re

port = "5555"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

topic = 53



def main(socket, oration_delimiter="..."):
    while True:
        # print "which file shall I read?"
        # fileName=raw_input("-->")
        filename="oratory1.txt"
        messagedata=""
        # def print_and_publish(messagedata):
        #     messagedata=messagedata+oration_delimiter
        #     print "%d %s" % (topic, messagedata)
        #     time.sleep(0.1)
        #     socket.send("%d %s" % (topic, messagedata))
        #     time.sleep(0.1)
        #     return ""
        # with open(filename) as oration:
            
        #     for line in oration.readlines():
        #         for word in line.split(' '):
        #             messagedata=messagedata+" "+word
        #             if len(messagedata)>60:
        #                 messagedata=print_and_publish(messagedata)
        #         messagedata=print_and_publish(messagedata)
        #     messagedata=print_and_publish(messagedata)
        #     exit()
        sentence=""
        expression=re.compile(r"[\.\?!]")
        with open(filename) as oration:
            for line in oration:
                for word in line.split(' '):
                    sentence=sentence+" "+word
                    if expression.findall(word):
                       print "%d %s" % (42, sentence)
                       socket.send("%d %s" % (42, sentence))
                       sentence=""
        exit()

if __name__=="__main__":
    context = zmq.Context()
    socket = context.socket(zmq.PUB)
    socket.bind("tcp://*:%s" % port)
    oration_delimiter="..."
    main(socket, oration_delimiter)
