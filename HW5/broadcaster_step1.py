import zmq
import random
import sys
import time

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

while True:
    topic = 42
    messagedata = "some string I found"
    # change to replacing strings "%s" 
    # instead of replacing decimal "%d"
    print "%d %s" % (topic, messagedata)
    socket.send("%d %s" % (topic, messagedata))
    # now we send strings
    time.sleep(1)