import sys
import zmq
import random
import bisect

class MarkovHistogram:
	class SecondWordHistogram:
		def __init__(self,second_word):
			self.second_words={second_word:1}
			self.cdf=[0.0]
			self.is_cdf_valid=True
			self.count=1
		def add_word(self,second_word):
			self.count+=1
			self.is_cdf_valid=False
			if not self.second_words.has_key(second_word):
				self.second_words[second_word]=1
			else:
				self.second_words[second_word]+=1

		def generate_cdf(self):
			self.cdf=[]
			reciprocal_count = 1.0/float(self.count)
			cumulative_probability=0.0
			for word in self.second_words.keys():
				cumulative_probability+=self.second_words[word]*reciprocal_count
				self.cdf.append(cumulative_probability)
			self.is_cdf_valid=True

		def random(self):
			if not self.is_cdf_valid:
				self.generate_cdf()
			rand_num=random.random()
			index=bisect.bisect(self.cdf,rand_num)
			return self.second_words.keys()[index-1]

		def __str__(self):
			return self.second_words.__str__()

	def __init__(self):
		""" Markov Histogram for monogram processing. 
		Primary data structure self.first_word_map 
		maps first words to SecondWordHistogram objects """
		self.first_word_map={} # primary data structure
		self.END_SYMBOL="__end__" # indicates finish of a sentence
		self.START_SYMBOL="__start__" # indicates start of a sentence
		self.state=self.START_SYMBOL # self.state represents the current state machine state
		# self.state transitions between words when next() is called

	def add_word_pair(self, first, second):
		""" adds a word pair to the histogram """
		if not self.first_word_map.has_key(first):
			self.first_word_map[first]=MarkovHistogram.SecondWordHistogram(second)
		else:
			self.first_word_map[first].add_word(second)

	def add_starting_word(self,starting_word):
		""" adds a word which has been used to start a sentence """
		self.add_word_pair(self.START_SYMBOL,starting_word)

	def add_ending_word(self,ending_word):
		""" indicates that a word has been used as the last word in a sentence """
		self.add_word_pair(ending_word, self.END_SYMBOL)

	def __iter__(self):
		""" indicates that this object can be iterated over """
		return self

	def next(self):
		""" generates random words using the histogram's distribution. Modifies self.state """
		self.state=self.first_word_map[self.state].random()
		if self.state==self.END_SYMBOL:
			self.state=self.START_SYMBOL
			raise StopIteration()
		return self.state

	def __str__(self):
		""" generates a printable string which represents the internal histogram data """
		string="MarkovHistogram:{"
		for first_word in self.first_word_map.keys():
			second_word_histogram=self.first_word_map[first_word]
			string=string+'"'+first_word+'":'+str(second_word_histogram)+","
		string=string[:-1]+"}"

		return string


def setup_zmq(port = "5556", ip_addr = "127.0.0.1", channel = 42):
	""" sets up a zmq socket to listen in on the broadcast """
	context = zmq.Context()
	socket = context.socket(zmq.SUB)

	print "Listening in to the broadcast..."
	socket.connect ("tcp://%s:%s" % (port,ip_addr))

	# filter by channel
	topicfilter = "%d" % channel
	socket.setsockopt(zmq.SUBSCRIBE, topicfilter)
	return socket

def main():
	""" builds a markov histogram by reading from a channel,
	then automatically generates 'Postmodernist wisdom' by 
	randomly walking through the markov model """

	socket=setup_zmq()
	hist=MarkovHistogram()
	for sentences_read_from_broadcast in range(30):
	    string = socket.recv()
	    words = string.split()[1:]
	    # add sentence to the histogram
	    hist.add_starting_word(words[0]) # add first word
	    for first,second in zip(words[0:-1],words[1:]):
	    	hist.add_word_pair(first,second) # add each pair of words
	    hist.add_ending_word(words[-1]) # add last word
	    # print the sentence
	    print " ".join(words)

	print "Postmodernist wisdom:"
	# the following structure: (a for a in a_iterable) produces a generator
	print " ".join((word for word in hist))
	# the structure [b for b in b_iterable] produces a list. This is a list comprehension
	print " ".join([word for word in hist])
	# both have the same function in this case.
	# generators can be more efficient than lists since they are not actually storing every element
	# rather they produce the desired element when called upon to provide it.
	# generators can be iterated through only once
	# but lists reset after you reach the end, and can be iterated multiple times
	

if __name__=="__main__":
	main()