import zmq
import random
import sys
import time

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

fileName="fake_post_modernist_scholarship.txt"
with open(fileName) as myFile:
	for line in myFile:
		for word in line.split():
			print word

# while True:
#     topic = 42
#     messagedata = "some string I found"
#     print "%d %s" % (topic, messagedata)
#     socket.send("%d %s" % (topic, messagedata))
#     time.sleep(1)