import zmq
import random
import sys
import time

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

fileName="fake_post_modernist_scholarship.txt"
topic = 42
with open(fileName) as myFile:
	for line in myFile:
	    print "%d %s" % (topic, line)
	    socket.send("%d %s" % (topic, line))
		# for word in line.split():
		# 	print word

# while True:
#     messagedata = "some string I found"
#     time.sleep(1)