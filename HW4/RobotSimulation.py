from SerialRobot import *
from RobotForces import *
import matplotlib.pyplot as plt
import matplotlib.animation as animation

# Robot simulation keeps track of the state:
class RobotSimulation():
	def __init__(self, dt_, robot_=SerialRobot("default"),forces_=[]):
		#These members store the simulation parameters
		#The robot and the robot forces should be initizialized outside of th simulation
		self.robot = robot_
		self.forces = forces_
		self.dt = dt_
		self.initialized = False

		#These are used to keep track of external forces
		#TODO: Make more efficient...don't multiply a bunch of zeros
		self.rowCt = 0
		for ii in range(0,len(self.forces)):
			F = self.forces[ii].getForce() #all forces represented in 2d
			self.rowCt += 2*len(F)

		self.concatForce = np.zeros(self.rowCt)
		self.concatForceJac = np.zeros((self.rowCt,self.robot.ndofs))
		self.M = self.robot.getMassMatrix()
		self.g = self.robot.getGravity()

		#These arrays will grow with every simulation step
		self.q = []
		self.qdot = []
		self.qddot = []
		self.time = []	

		#For plotting and animation
		self.fig = plt.figure()
		self.ax = self.fig.gca()	
		self.xlim = [-0.5,0.5]
		self.ylim = [-0.05,1.0]

	#advances the simulation by the specified number of Newmark-Beta steps
	#with a bit of a hack on the damping term in the force.
	#OPTIONAL: use built-in integrator
	def step(self, nsteps_):
		if self.initialized:
			for ii in range(0,nsteps_):
				#find new joint positions
				q = self.q[-1] + self.dt*self.qdot[-1] + (0.5*self.dt**2)*self.qddot[-1]
				#find new joint accelerations based on new positions
				qddot = self.__getQddot(q,self.qdot[-1]) #using old velocity but new position...ugh...
				#find new velocities
				qdot = self.qdot[-1] + 0.5*self.dt*(self.qddot[-1] + qddot)
				#bookkeeping
				t = self.time[-1] + self.dt
				self.q.append(q)
				self.qdot.append(qdot)
				self.qddot.append(qddot)
				self.time.append(t)
		else: 
			print "WARNING: NO STEPS UNTIL INITIZED"

	def __updateForces(self):
		for ii in range(0,len(self.forces)):
			self.forces[ii].update(self.robot)
			Fii = self.forces[ii].getForce()
			Jii = self.forces[ii].getJacobian()
			for jj in range(0,len(Fii)):
				sind = ii*2+jj*2
				self.concatForce[sind:sind+2] = Fii[jj]
				self.concatForceJac[sind:sind+2,:] = Jii[jj]

	def __updateInertial(self):
		raise NotImplementedError()
		#TODO: update self.M and self.g using self.robot method calls

	def __getQddot(self, q_, qdot_):
		self.robot.setState(q_,qdot_)
		self.robot.update()
		self.__updateForces()
		self.__updateInertial()
		#TODO: solve qddot = M^-1 * rhs (see assignment) and return qddot

	def initialize(self, q0_, qdot0_):
		#IMPROVE: size check!
		if not self.initialized:
			self.q.append(q0_)
			self.qdot.append(qdot0_)
			qddot = self.__getQddot(q0_,qdot0_)			
			self.qddot.append(qddot)
			self.time.append(0)	
			self.initialized = True
		else:
			print "WARNING: CANNOT REINITIALIZE"

	def getSimData(self):
		return [self.time, self.q, self.qdot, self.qddot]

	def animate(self):
		print len(self.q)
		self.__calcLimits(0.1)
		self.anim = animation.FuncAnimation(self.fig,self.__animateFunction,
										frames=range(len(self.q)),
										init_func=self.__setAxes, interval=self.dt*1000,repeat=False)
		plt.show()

	def __animateFunction(self,i):
		plt.cla()
		self.__setAxes
		self.robot.setState(self.q[i],self.qdot[i])
		self.robot.update()
		self.robot.draw(self.fig.gca())
		###HACK HACK HACK###
		#We should really add visuals to the robotForce objects
		#this is a stand-in floor-drawing for this demo
		###HACK HACK HACK###
		vtemp = [[self.xlim[0],0],[self.xlim[1],0]]
		codes = [pth.Path.MOVETO,pth.Path.LINETO]
		path = pth.Path(vtemp,codes)
		floor = patches.PathPatch(path,facecolor='k',lw=2)
		self.fig.gca().add_patch(floor)
		return self.fig.gca()

	def __setAxes(self):
		self.fig.gca().set_xlim(self.xlim[0],self.xlim[1])
		self.fig.gca().set_ylim(self.ylim[0],self.ylim[1])
		self.fig.gca().set_aspect('equal')

	#determines axis limits by using a buffer that is ~cch size of each link
	#IMPROVE: update shapes to return their  own limits and use this
	def __calcLimits(self,buffer_=0.1):
		self.xlim=[0.0,0.0]
		self.ylim=[0.0,0.0]
		for tt in range(0,len(self.q)):
			self.robot.setPosition(self.q[tt])
			self.robot.update()
			for jj in range(0,self.robot.ndofs):
				T_jj = self.robot.getTransformAsMatrix(jj)
				if T_jj[0,2]-buffer_ < self.xlim[0]:
					self.xlim[0] = T_jj[0,2]-buffer_

				if T_jj[0,2]+buffer_ > self.xlim[1]:
					self.xlim[1] = T_jj[0,2]+buffer_

				if T_jj[1,2]-buffer_ < self.ylim[0]:
					self.ylim[0] = T_jj[1,2]-buffer_

				if T_jj[1,2]+buffer_ > self.ylim[1]:
					self.ylim[1] = T_jj[1,2]+buffer_





