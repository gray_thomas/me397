#Set of classes to enable dynamic simulations of serial mult-body
#systems in 2d for HW4 of ME397
import numpy as np


#Encapuslation of a 2d rigid body transform from F0 to F1
#d_ is offset from F0 to F1, represented in F0
#theta_ parametrizes rotation from F0 to F1 
#s.t. x_1 {represented in F0} = R(theta_) x_0
class Transform:
	def __init__(self,d_=np.zeros(2),theta_=0.0):
		# copy initial data to local variables
		self.d = d_
		self.theta = theta_
		# Setup appropriately sized np.arrays for other variabls.
		self.R = np.zeros((2,2))
		# transforms a point represented in F1 
		# to its equivalent representation in F0
		self.T_1to0 = np.zeros((3,3))
		self.T_1to0[2,2] = 1.0
		# transforms a point represented in F0 
		# to its equivalent representation in F1
		self.T_0to1 = np.zeros((3,3))		
		self.T_0to1[2,2] = 1.0
		# Call update to initialize transforms
		# self.__update()

	def __update(self):
		#set rotation matrix R(theta)
		#TODO
		#set self.T_1to0 
		#TODO
		#set self.T_0to1
		#TODO
		raise NotImplementedError()
		
	def updateTheta(self,theta_):	
		self.theta = theta_
		self.__update()

	def updateDisplacement(self,d_):
		self.d = d_.squeeze()
		self.__update()

	def getSE2(self):
		return self.T_0to1

	#This is most commonly used to transform joint frame to inertial frame
	def getInverseSE2(self):
		return self.T_1to0