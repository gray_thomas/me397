import numpy as np
import matplotlib.pyplot as plt
import matplotlib.path as pth
import matplotlib.patches as patches
import matplotlib.path as path
import matplotlib.animation as anim
from math import sin, cos

from Transform import *
#Base class for shapes (which will )
#All shapes are defined w.r.t. a link (parent jonit) frame, 
#with an initial (static) rigid body transform (T0) relative to that frame 
#IMPROVE: make this an abstract base class
class Shape:
    def __init__(self,name_,transform_=Transform(np.zeros(2),0.0)):
        self.name = name_
        self.T0 = transform_

    def getStaticTransform(self):
        return self.T0

    #update internal variables from a transform object
    #baseTransform maps the parent frame to the inertial frame
    def update(self, baseTransform_=Transform(np.zeros(2),0.0)):
        #updates internal variables in preparation for drawing a figure
        raise NotImplementedError()

    #update internal variables from a transform represented as a matrix
    #baseTransform maps the parent frame to the inertial frame
    def updateFromMatrix(self, baseTMatrix_):
        #updates internal variables in preparation for drawing a figure
        raise NotImplementedError()

    #draw adds the object (e.g. a patch) to the axis ax_
    def draw(self, ax_):
        #draws the object
        raise NotImplementedError()

#Line from p1 to p2
class Line(Shape):
    def __init__(self,name_,p1_,p2_,transform_=Transform(np.zeros(2),0.0)):
        Shape.__init__(self,name_,transform_)
        #initialize member data
        self.p1 = p1_
        self.p2 = p2_
        #initial vertices
        self.v0 = [p1_,p2_]
        #fully transformed vertices
        self.vertices = [p1_,p2_]
        self.update()

    def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
        baseT = baseTransform_.getInverseSE2()
        self.updateFromMatrix(baseT)

    def updateFromMatrix(self, baseTMatrix_):
        T0 = np.dot(baseTMatrix_,self.T0.getInverseSE2())
        self.vertices[0] = np.dot(T0[0:2,0:2],self.p1) + T0[0:2,2]
        self.vertices[1] = np.dot(T0[0:2,0:2],self.p2) + T0[0:2,2]

    def draw(self, ax_, color_='r',lw_=2):
        #Note: use of patch not completely necessary...but useful hint for rectangle implementation
        vtemp = self.vertices[:]
        codes = [pth.Path.MOVETO,pth.Path.LINETO]
        path = pth.Path(vtemp,codes)
        patch = patches.PathPatch(path,facecolor=color_,lw=lw_)
        ax_.add_patch(patch)

#Rectangle centered at (0,0) with width in x-direction and height in y-direction
class Rectangle(Shape):
    def __init__(self,name_,width_,height_,transform_=Transform(np.zeros(2),0.0)):
        Shape.__init__(self,name_,transform_)
        self.width = width_
        self.height = height_
        h2=height_*0.5
        w2=width_*0.5
        self.v0 = np.array([[w2,-w2,-w2,w2,w2],[h2,h2,-h2,-h2,h2]])
        self.vertices = np.zeros((5,2))

        self.codes = np.ones(5, int) * path.Path.LINETO
        self.codes[0] = path.Path.MOVETO
        self.codes[4] = path.Path.CLOSEPOLY
        self.rectangle_path=path.Path(self.vertices, self.codes)
        self.updateFromMatrix(np.identity(3))

    def setMatrixFetchFunction(self, matrix_fetcher):
        # add a matrix_fetcher member, which is a function
        self.fetch_matrix=matrix_fetcher

    def animate(self):
        # call matrix_fetcher as a function
        self.updateFromMatrix(self.fetch_matrix())

    def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
        baseT = baseTransform_.getInverseSE2()
        self.updateFromMatrix(baseT)

    def updateFromMatrix(self, baseTMatrix_):
        for i in range(0,5):
            self.vertices[i,:]=(baseTMatrix_[0:2,0:2].dot(self.v0[:,i])
                    +baseTMatrix_[0:2,2]).T

    def draw(self, ax_, color='green', **kwargs):
        self.patch = patches.PathPatch(self.rectangle_path,
                 facecolor=color, edgecolor='yellow', alpha=0.5, **kwargs)
        ax.add_patch(self.patch)

class Circle(Shape):
    def __init__(self,name_,radius_,transform_=Transform(np.zeros(2),0.0)):
        Shape.__init__(self,name_,transform_)
        self.radius = radius_
        self.center = np.zeros(2)
        self.update()

    def update(self,baseTransform_=Transform(np.zeros(2),0.0)):
        baseT = baseTransform_.getInverseSE2()
        self.updateFromMatrix(baseT)

    def updateFromMatrix(self, baseTMatrix_):
        raise NotImplementedError()
        # Calculate net transform T0 and apply it to the center of the circle
        # TODO      

    def draw(self,ax_,color_='r'):
        raise NotImplementedError()
        # TODO: create a circular patch and add it ax_
        
if __name__=="__main__":
    print "hello world"
    fig, ax=plt.subplots()
    ax.axis('equal')
    ax.axis([-1,1,-1,1])
    rect=Rectangle("rect 1", 0.4, 0.4)
    rect.draw(ax)
    trans=np.array([ [1.0, 0.0, 0.0],
                    [0.0, 1.0, 0.0],
                    [0.0, 0.0, 1.0]])
    fetch_transform_lambda = lambda : trans
    rect.setMatrixFetchFunction(fetch_transform_lambda)
    # make a list of animatable objects
    animatables=[rect]

    def set_pose(i):
        theta=i*0.1+2
        x=0.0+0.3*sin(i*0.01)
        y=0.0
        trans[0:2,0:3]=np.array([
            [cos(theta),-sin(theta),x],
            [sin(theta),cos(theta),y]
            ])

    # obtain the threading and time libraries for starting
    # new threads and pausing on the real clock.
    import thread
    import time

    # define a number which loop poses can use to remember
    # where it is in the list of poses
    pose_number=0

    # define a function which iterates over the poses using
    # the pose_number variable like a global
    def loop_poses():
        # pose_number cannot be modified through side effects
        global pose_number
        if pose_number>=50:
            pose_number=0
        set_pose(pose_number)
        pose_number+=1

    # define a funciton which is the main funciton for the new
    # thread, calling loop_poses and waiting briefly
    def loop_poses_forever():
        while True:
            loop_poses()
            time.sleep(0.02)

    # this is the call which starts your new thread. The ()
    # means you don't need to pass any arguments to 
    # loop_poses_forever.
    thread.start_new_thread(loop_poses_forever, ())
    def animate(i):
        # now just animate the objects.
        for animatable in animatables:
            animatable.animate()
    # and setup the animation
    ani=anim.FuncAnimation(fig, animate, range(0,50),interval=20)
    # plt.show is unlike normal theads in that it halts your execution
    plt.show()
