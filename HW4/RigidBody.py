import numpy as np
from Transform import *
from Shape import *

#Base class for inertial parameters of rigid bodies
#I is defined about center of mass unless otherwise noted
class Inertia:
	def __init__(self,name_,M_,I_,COM_):
		self.name = name_
		self.M = 0.0
		self.I = 0.0
		self.COM = np.zeros(2) 
		self.setParameters(M_,I_,COM_)

	def setParameters(self,M_,I_,COM_):
		self.M = M_
		self.I = I_
		self.COM = COM_

	def setName(self,name_):
		self.name = name_

	def getCOM(self):
		return self.COM

	def getM(self):
		return self.M

	def getI(self):
		return self.I


#Links contain all information about
#the inertial parameters, visualization, and collision shapes of the body
#It is only necessary to define inertial parameters--collisions and visuals can be left off
#The reference frame is defined by parent (joint) index
#There is a 1:1 relationship between joints and links
class Link:
	def __init__(self,name_,inertia_=Inertia("default",0.0,0.0,np.zeros((2,1)))):
		self.name = name_
		self.collisions = []
		self.visuals = []
		#This needs to be defined w.r.t. the parent joint frame, NOT the center of mass
		#However, the inertia passed in represents the inertia w.r.t. the COM--renaming local
		#inertia to reflect that it is not the same as the original link inertia
		Iname = name_+"_inertia"
		inertia_.setName(Iname)
		self.inertia = inertia_
		self.__setInertia(inertia_)

	#IMPROVE: accept a list of inertias, calculate net inertia.
	def __setInertia(self,inertia_):
		#TODO: parallel axis theorem so rotational inertia is defined w.r.t.the joint frame
		#I_joint <-- I_com + d^2*M
		raise NotImplementedError()

	def setVisuals(self,visuals_=[]):
		self.visuals = visuals_

	def setCollisions(self,collisions_=[]):
		self.collisions = collisions_

#A joint defined by parent index (-1 for the first joint), 
#the transform from that parent,
#the link attached to the joint,
#and an axis index (i.e., direction of motion with 0=theta, 1=x, 2=y)
#so that joint motion restricted to cardinal axes
class Joint:
	def __init__(self, name_, axisIdx_=0, parentIdx_=-1, transform_=Transform(np.zeros(2),0.0), link_=Link("default")):
		self.name = name_
		self.parentIdx = parentIdx_
		self.axisIdx = axisIdx_
		#initial static transform
		self.T0 = transform_
		#internal transform relative to initial transform--updated as joint moves
		#and depends ONLY on joint axis.  Initially identity. Full joint transform given by T0 * TJ 
		self.TJ = Transform(np.zeros(2),0.0)
		self.link = link_

	def setLink(self, link_):
		self.link = link_

	def getIdx(self):
		#TODO: return this joint's index based on parent index (and npn-branching assumption)
		raise NotImplementedError()

	def getAxIdx(self):
		return self.axisIdx

	def getName(self):
		return self.name

	def update(self,q):
		if self.axisIdx == 0: #revolute joint
			#TODO: update self.TJ for a revolute joint
			raise NotImplementedError()
		else: #prismatic joint 
			#TODO: update self.TJ
			raise NotImplementedError()

	def getTransformAsMatrix(self):
		# return full joint transform given by T0 * TJ 
		raise NotImplementedError()