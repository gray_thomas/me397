from RigidBody import *

#Robot is defined by a list of joints, each with a parent joint and chilc link
#The joint list should be passed in at initialization, by default gravity is not present
class SerialRobot:
	def __init__(self, name_, joints_=[], grav_=np.zeros(2)):
		self.name = name_
		self.joints = joints_
		self.ndofs = len(joints_)
		self.q = np.zeros(self.ndofs)
		self.qdot = np.zeros(self.ndofs)
		self.command = np.zeros(self.ndofs)
		self.grav = grav_
		self.transforms = []
		if(len(joints_))>0:
			self.__setup()
		else:
			print "WARNING: BLANK ROBOT"

	#setup sorts the joint list so they are in the correct order
	#IMPROVE: this method could use some more logic and stiffer error checks
	def __setup(self):
		jtemp = self.joints[:]
		#TODO: (using jtemp) sort the list of joints so order is 0-->N
		#if multiple joints have the same index, this overwrites
		
		#TODO: itialize self.transforms to
		#list of recursive transforms for each frame mapping 
		#(point in joint frame)-->(point in inertial reference)
		#self.transforms[j]  = T[j] = T[0]*T[1] * ... * T[j]
		self.joints = jtemp

	#returns Jacobian so that t = J * qdot, where t = (thetadot, xdot, ydot)
	def getJacobian(self,jointIdx_,point_=np.zeros(2)):
		#initialize Jacobian of size 3xndofs to zero
		J = np.zeros((3,self.ndofs))
		#calculate the point inthe inertial reference frame
		T = self.transforms[jointIdx_]
		p_inertial = np.dot(T[0:2,0:2],point_) + T[0:2,2]
		#set each column of the Jacobian accounting for joint type
		for jj in range(0,jointIdx_+1):
			Tjj = self.transforms[jj]
			if self.joints[jj].getAxIdx() == 0: #revolute joint
				p_jj_inertial = Tjj[0:2,2]
				p_arm = p_inertial - p_jj_inertial
				J[0,jj] = 1.0
				J[1,jj] = p_arm[1]
				J[2,jj] = -p_arm[0]
			else: #prismatic joint
				axis_joint = np.zeros((2,1))
				axis_joint[self.joints[jj].getAxIdx()-1] = 1.0
				axis_inertial = np.dot(Tjj[0:2,0:2],axis_joint)
				J[1,jj] = axis_inertial[0]
				J[2,jj] = axis_inertial[1] 
		return J

	# calculates and returns configuration dependent joint space mass matrix
	def getMassMatrix(self):
		M = np.zeros((self.ndofs,self.ndofs))
		#TODO: calcualte mass joint space matrix
		return M

	# calculates and returns joint-space gravity vector
	def getGravity(self):
		g = np.zeros(self.ndofs)
		#TODO : calculate joint space gravity vector
		return g

	#returns the point_ represented in inertial reference frame
	def getPoint(self,jointIdx_,point_=np.zeros(2)):
		#TODO: use self.transforms to calculate p_inertial = Tj * point_
		return p_inertial

	def getTransformAsMatrix(self,jointIdx_):
		T = self.transforms[jointIdx_]
		return T

	def setPosition(self,q_):
		self.q = q_

	def setVelocity(self,qdot_):
		self.qdot = qdot_

	def setState(self,q_,qdot_):
		self.q = q_
		self.qdot = qdot_

	def setCommand(self, command_):
		startIdx = self.ndofs-len(command_)
		self.command[startIdx:len(command_)] = command_

	def getJoint(self,jointIdx_):
		return self.joints[jointIdx_]

	def getLink(self,jointIdx_):
		return self.joints[jointIdx_].link

	# updates each robot joint and recursive transform to that joint
	def update(self):		
		for jj in range(0,len(self.joints)):
			raise NotImplementedError()
			#Update individual joints
			#TODO: update joint jj using self.q[jj]
			#TODO: update recursive transform self.transform[jj]

	def draw(self,ax_,colColor_='r',visColor_='g'):
		for jj in range(0,len(self.joints)):
			link_jj = self.getLink(jj)
			for kk in range(0,len(link_jj.visuals)):
				link_jj.visuals[kk].updateFromMatrix(self.transforms[jj])
				link_jj.visuals[kk].draw(ax_,visColor_)
			for ll in range(0,len(link_jj.collisions)):
				link_jj.collisions[ll].updateFromMatrix(self.transforms[jj])
				link_jj.collisions[ll].draw(ax_,colColor_)