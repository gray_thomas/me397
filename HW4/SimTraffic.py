from TrafficSignal import *
class SimTraffic:

	def __init__(self):
		pass

	def newSimulation(self):
		man= TrafficSignal.Manager()
		man.newTrafficSignal(-1,10)
		man.newTrafficSignal('a',10)
		man.newTrafficSignal('44 -44',10)
		print man.getSignals()

		# create traffic signals with one or more faces
		# register signals for state changes
		# handle signal and time notifications
def main():
	SimTraffic().newSimulation()


if __name__=="__main__":
	main()
