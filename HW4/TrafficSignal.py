class TrafficSignal:

	def __init__(self, faces, sensitivity):
		self.__faces = faces
		self.__sense = sensitivity

		if faces<1:
			raise Exception("impossible number of faces")

		for ii in range (0, faces):
			print "creating a new face"

	def getFaces(self):
		return self.__faces
	def setPeriod(self, seconds):
		self.__period = seconds
	def getPeriod(self):
		return self.__period

	class Manager:
		def __init__(self):
			self.list_of_traffic_signals=[]

		def getSignals(self):
			return self.list_of_traffic_signals

		def newTrafficSignal(self, faces, sensitivity):
			# handle general exceptions
			try:
				ts=TrafficSignal(faces, sensitivity)
				self.list_of_traffic_signals.append(ts)
			except Exception, e:
				print "Excepetion on TrafficSignal.Manager is ", e