from SerialRobot import *
from RobotForces import *

#################################################
###### Virtual Joints ###########################
#################################################
#first three joints are the virtual joints so all relevant information
#about the "base" body is set for the child link of the 3rd joint (here, L2)
#x-direction virtual joint
I0=Inertia("I0",0,0,np.zeros(2))
L0=Link("link0",I0)
T0=Transform()
J0=Joint("joint0_virtual",1,-1, T0, L0)

#y-direction virtual joint
I1=Inertia("I1",0,0,np.zeros(2))
L1=Link("link1",I1)
T1=Transform(np.zeros(2),0.0)
J1=Joint("joint1_virtual",2, 0, T1, L1)

#theta-direction virtual joint
#Inertia for floating base
I2=Inertia("I2",5,1,np.zeros(2))
L2=Link("link2_base",I2)
#Visual geometry is a rectangle
S2_vis=[Rectangle("r0",0.2,0.1)]
L2.setVisuals(S2_vis)
#Collision geometries are circles at each corner
T2_corner1 = Transform(np.array([-0.1,-0.05]),0.0)
T2_corner2 = Transform(np.array([-0.1,0.05]),0.0)
T2_corner3 = Transform(np.array([0.1,0.05]),0.0)
T2_corner4 = Transform(np.array([0.1,-0.05]),0.0)
S2_collision=[Circle("c0",0.01,T2_corner1),
			  Circle("c1",0.01,T2_corner2),
			  Circle("c2",0.01,T2_corner3),
			  Circle("c3",0.01,T2_corner4)]
L2.setCollisions(S2_collision)
T2=Transform(np.zeros(2),0.0)
J2=Joint("joint2_virtual",0, 1, T2, L2)

#################################################
###### Real Joints ##############################
#################################################
I3=Inertia("I3",2,1,np.array([0.1,0]))
L3=Link("link3_real",I3)
#Visual geometry is a line
S3_vis=[Line("l0",np.array([0,0]),np.array([0,0.2]))]
L3.setVisuals(S3_vis)
#Collision geometry is a circle at the tip of the line
T3_tip=Transform(np.array([0,0.2]),0.0)
S3_collision=[Circle("c0",0.01,T3_tip)]
L3.setCollisions(S3_collision)
T3=Transform(np.array([0.0,0.05]),0)
J3=Joint("joint3_real",0,2,T3,L3)

I4=Inertia("I4",4,1,np.array([0.15,0]))
L4=Link("link4_real",I4)
#Visual geometry is a line
S4_vis=[Line("l0",np.array([0,0]),np.array([0,0.3]))]
L4.setVisuals(S4_vis)
#Collision geometry is a circle at the tip of the line
T4_tip=Transform(np.array([0,0.29]),0.0)
S4_collision=[Circle("c0",0.01,T4_tip)]
L4.setCollisions(S4_collision)
T4=Transform(np.array([0.0,0.2]),0)
J4=Joint("joint4_real",0,3,T4,L4)

I5=Inertia("I5",3,1,np.array([0.05,0.025]))
L5=Link("link5_real",I5)
#Visual geometry is a rectangle
S5_vis=[Rectangle("r0",0.1,0.05)]
L5.setVisuals(S5_vis)
#Collision geometries are circles at each corner
T5_corner1 = Transform(np.array([-0.05,-0.025]),0.0)
T5_corner2 = Transform(np.array([-0.05,0.025]),0.0)
T5_corner3 = Transform(np.array([0.05,0.025]),0.0)
T5_corner4 = Transform(np.array([0.05,-0.025]),0.0)
S5_collision=[Circle("c0",0.01,T5_corner1),
			  Circle("c1",0.01,T5_corner2),
			  Circle("c2",0.01,T5_corner3),
			  Circle("c3",0.01,T5_corner4)]
L5.setCollisions(S5_collision)
T5=Transform(np.array([0.0,0.3]),0)
J5=Joint("joint5_real",0,4,T5,L5)

#Build the robot
legRobot = SerialRobot("floatingRobot",[J1,J0,J2,J3,J4,J5],np.array([0,-9.8]))

#Show the zero configuration
f = plt.figure()
f.gca().set_xlim(-.5,.5)
f.gca().set_ylim(-0.2,1)
f.gca().set_aspect('equal')
legRobot.draw(f.gca())
plt.show()

#Set up the robot forces from contact and springs
floorNormal = np.array([0,1])
floorPoint = np.zeros(2)
rootIdx = 2 #index of the parent joint of the first real body
Base_contact = ExternalContact("Base_contact",floorPoint,floorNormal,100000,1000,rootIdx,legRobot)
L1_contact = ExternalContact("L1_contact",floorPoint,floorNormal,100000,1000,rootIdx+1,legRobot)
L2_contact = ExternalContact("L2_contact",floorPoint,floorNormal,100000,1000,rootIdx+2,legRobot)
L3_contact = ExternalContact("L3_contact",floorPoint,floorNormal,100000,1000,rootIdx+3,legRobot)

# Neutral configuration for springs
q0 = np.array([0.0,0.051,0.0,-np.pi/12,np.pi/6,-np.pi/12])
legRobot.setPosition(q0)
legRobot.update()

m1Anchor_base = np.array([-0.05,0.05])
m1Anchor_L1 = np.array([0.0,0.05])
ankle_muscle0 = LinearMuscle("ankle0",5000,rootIdx,m1Anchor_base,
						   rootIdx+1,m1Anchor_L1,0.0,legRobot)

m1Anchor_base = np.array([0.05,0.05])
ankle_muscle1 = LinearMuscle("ankle1",5000,rootIdx,m1Anchor_base,
						   rootIdx+1,m1Anchor_L1,0.0,legRobot)


m2Anchor_L1 = np.array([0.0,0.15])
m2Anchor_L2 = np.array([0,0.05])
knee_muscle = LinearMuscle("knee",5000,rootIdx+1,m2Anchor_L1,
						   rootIdx+2,m2Anchor_L2,0.0,legRobot)

m3Anchor_L2 = np.array([0.0,0.25])
m3Anchor_L3 = np.array([-0.025,0.0])
hip_muscle = LinearMuscle("hip",5000,rootIdx+2,m3Anchor_L2,
						   rootIdx+3,m3Anchor_L3,0.0,legRobot)

legRobotForces=[Base_contact,L1_contact,L2_contact,L3_contact,
				ankle_muscle0, ankle_muscle1, knee_muscle, hip_muscle]

# legRobotForces=[Base_contact,L1_contact,L2_contact,L3_contact]
