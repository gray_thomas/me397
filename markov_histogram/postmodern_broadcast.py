import zmq
import random
import sys
import time
import re

port = "5556"
if len(sys.argv) > 1:
    port =  sys.argv[1]
    int(port)

context = zmq.Context()
socket = context.socket(zmq.PUB)
socket.bind("tcp://*:%s" % port)

fileName="fake_post_modernist_scholarship.txt"
topic = 42
# incrementally building a message
message_data=""
sentence_end_finder=re.compile(r"[\.\?!]")
with open(fileName) as myFile:
    for line in myFile:
        for word in line.split():
            message_data=message_data+" "+word
            # print message data when a sentence ends
            if sentence_end_finder.findall(word):
                print "%d %s" % (topic, message_data)
                socket.send("%d %s" % (topic, message_data))
                message_data=""
                time.sleep(1)
        