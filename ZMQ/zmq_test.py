import time
import thread
import zmq

context = zmq.Context()
addressA = "tcp://127.0.0.1:5555"
addressB = "tcp://127.0.0.1:5556"
def producer():
    try:
        zmq_socket = context.socket(zmq.PUSH)
        zmq_socket.bind(addressA)
        for num in xrange(20000):
            work_message = {'num': num}
            zmq_socket.send_json(work_message)
            time.sleep(0.001)
    except Exception as e:
        print "producer exception is ", e

thread.start_new_thread(producer,())
import random

def consumer():
    try:
        consumer_id = random.randrange(1,10005)
        print "I am consumer #%s" % (consumer_id)
        # context = zmq.Context()

        consumer_receiver = context.socket(zmq.PULL)
        consumer_receiver.connect(addressA)

        consumer_sender = context.socket(zmq.PUSH)
        consumer_sender.connect(addressB)

        while True:
            work = consumer_receiver.recv_json()
            data = work['num']
            result = { 'consumer' : consumer_id, 'num' : data}
            if data%2 ==0:
                consumer_sender.send_json(result)
    except Exception as e:
        print "consumer exception is :", e
thread.start_new_thread(consumer,())
thread.start_new_thread(consumer,())
thread.start_new_thread(consumer,())
thread.start_new_thread(consumer,())
thread.start_new_thread(consumer,())

import pprint

def result_collector():
    # context = zmq.Context()
    try:
        results_receiver = context.socket(zmq.PULL)
        results_receiver.bind(addressB)
        collector_data = {}
        for x in xrange(1000):
            result = results_receiver.recv_json()
            if collector_data.has_key(result['consumer']):
                collector_data[result['consumer']] = collector_data[result['consumer']]+1
            else :
                collector_data[result['consumer']]=1
            if x%10 == 0:
                pprint.pprint(collector_data)
    except Exception as e:
        print "collector error is", e

result_collector()